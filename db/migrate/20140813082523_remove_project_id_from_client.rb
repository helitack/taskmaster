class RemoveProjectIdFromClient < ActiveRecord::Migration
  def change
    remove_column :clients, :project_id, :integer
  end
end
