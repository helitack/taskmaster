# == Schema Information
#
# Table name: projects
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  address_id :integer
#  client_id  :integer
#  created_at :datetime
#  updated_at :datetime
#

class Project < ActiveRecord::Base
	has_many :tasks, dependent: :destroy
	belongs_to :address
	belongs_to :client
end
