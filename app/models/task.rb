# == Schema Information
#
# Table name: tasks
#
#  id           :integer          not null, primary key
#  task_name    :string(255)
#  task_type_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#  project_id   :integer
#  completed_at :datetime
#

class Task < ActiveRecord::Base
	# Assocations
	belongs_to :task_type
	belongs_to :project


	# Validation
	validates :task_type_id, :project_id, presence: true
end
