# == Schema Information
#
# Table name: addresses
#
#  id          :integer          not null, primary key
#  street      :string(255)
#  city        :string(255)
#  province    :string(255)
#  postal_code :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class Address < ActiveRecord::Base
end
