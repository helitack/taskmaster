# == Schema Information
#
# Table name: clients
#
#  id          :integer          not null, primary key
#  first_name  :string(255)
#  middle_name :string(255)
#  last_name   :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class Client < ActiveRecord::Base
	has_one :project

	validates :first_name, :last_name, presence: true
end
