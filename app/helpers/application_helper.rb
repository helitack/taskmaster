module ApplicationHelper


	#
	# Bootstrap Flash Messages Formatting Helper
	def bootstrap_class_for flash_type
		{ success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || "alert-" + flash_type.to_s
	end

	def flash_messages(opts = {})
		flash.each do |msg_type, message|
			concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} animated fadeIn") do 
					concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
					concat message 
				end)
		end
		nil
	end

	#
	# Return active for navbar indicators
	def is_active(controller)
		"active" if current_page?( controller )
	end

	#
	# Return animations for navbar
	def root_animation
		if current_page?(root_path)
			"animated fadeInLeft"
		else
			"animated fadeIn"
		end
	end
end
