module TasksHelper

	def task_types
		{
			1 => "Check The Project Address",
			2 => "Ensure There Is A Client For This Project"
		}
	end

end