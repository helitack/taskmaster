json.array!(@tasks) do |task|
  json.extract! task, :task_name, :task_type_id
  json.url task_url(task, format: :json)
end