json.array!(@addresses) do |address|
  json.extract! address, :street, :city, :province, :postal_code
  json.url address_url(address, format: :json)
end