json.array!(@projects) do |project|
  json.extract! project, :name, :address_id, :client_id
  json.url project_url(project, format: :json)
end